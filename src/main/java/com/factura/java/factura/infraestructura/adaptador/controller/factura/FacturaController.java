package com.factura.java.factura.infraestructura.adaptador.controller.factura;

import com.factura.java.factura.aplicacion.comando.FacturaComando;
import com.factura.java.factura.aplicacion.manejador.factura.ManejadorCrearFactura;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("factura")
public class FacturaController {

    private ManejadorCrearFactura manejador;

    public FacturaController(ManejadorCrearFactura manejador){
        this.manejador = manejador;
    }

    @PostMapping("/crear")
    @ResponseStatus(HttpStatus.CREATED)
    public void crear(@RequestBody FacturaComando comando) {
        manejador.crear(comando);
    }
}
