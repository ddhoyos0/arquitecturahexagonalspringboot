package com.factura.java.factura.dominio.repositorio;

import com.factura.java.factura.dominio.modelo.Factura;

public interface FacturaRepositorio {
    public void crear(Factura factura);
}
